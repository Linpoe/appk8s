# Azure Provider source and version being used
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "K8S" {
  name     = "AppK8S"
  location = "westus3"
}

# # point to resource group
# data "azurerm_resource_group" "K8S" {
#     name =  "Groupe4"
# }



# Create a virtual network within the resource group
resource "azurerm_virtual_network" "K8S" {
  name                = "AppK8SVnet"
  resource_group_name = azurerm_resource_group.K8S.name
  location            = azurerm_resource_group.K8S.location
  address_space       = ["10.0.0.0/16"]

}

# # point to vnet
# data "azurerm_virtual_network" "K8S" {
#     resource_group_name = azurerm_resource_group.K8S.name
#     name =  "group4Vnet"
# }



resource "azurerm_ssh_public_key" "K8S" {
  name                = "AppK8SKey"
  resource_group_name = azurerm_resource_group.K8S.name
  location            = azurerm_resource_group.K8S.location
  public_key          = file("~/.ssh/id_rsa.pub")
}

# Create subnet
resource "azurerm_subnet" "K8S" {
  name                 = "AppK8Ssubnet"
  resource_group_name  = azurerm_resource_group.K8S.name
  virtual_network_name = azurerm_virtual_network.K8S.name
  address_prefixes     = ["10.0.1.0/24"]
}

# # point to subnet
# data "azurerm_subnet" "group4subnet" {
#   resource_group_name = azurerm_resource_group.K8S.name
#   virtual_network_name = azurerm_virtual_network.K8S.name
#   name = "group4subnet"
# }

# Create Network Security Group and rule
resource "azurerm_network_security_group" "K8S" {
  name                = "AppK8S-nsg"
  location            = azurerm_resource_group.K8S.location
  resource_group_name = azurerm_resource_group.K8S.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
#     security_rule {
#     name                       = "Jenkins"
#     priority                   = 101
#     direction                  = "Inbound"
#     access                     = "Allow"
#     protocol                   = "*"
#     source_port_range          = "*"
#     destination_port_range     = "8080"
#     source_address_prefix      = "*"
#     destination_address_prefix = "*"
#   }
#       security_rule {
#     name                       = "Slave1"
#     priority                   = 100
#     direction                  = "Inbound"
#     access                     = "Allow"
#     protocol                   = "*"
#     source_port_range          = "*"
#     destination_port_range     = "80"
#     source_address_prefix      = "*"
#     destination_address_prefix = "*"
#   }
}


resource "azurerm_public_ip" "MasterK8S" {
  name                = "Master-ip"
  resource_group_name = azurerm_resource_group.K8S.name
  location            = azurerm_resource_group.K8S.location
  allocation_method   = "Static"
}

# Create network interface
resource "azurerm_network_interface" "MasterK8S" {
  name                = "MasterNic"
  location            = azurerm_resource_group.K8S.location
  resource_group_name = azurerm_resource_group.K8S.name

  ip_configuration {
    name                          = "ipMaster"
    subnet_id                     = azurerm_subnet.K8S.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.MasterK8S.id
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "MasterK8S" {
  network_interface_id      = azurerm_network_interface.MasterK8S.id
  network_security_group_id = azurerm_network_security_group.K8S.id
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "MasterK8S" {
  name                  = "AppK8SMaster"
  location              = azurerm_resource_group.K8S.location
  resource_group_name   = azurerm_resource_group.K8S.name
  network_interface_ids = [azurerm_network_interface.MasterK8S.id]
  size                  = "Standard_B1ms"

  os_disk {
    name                 = "AppK8S_OsDisk_1_0e1a4191917b48d2b5aaf7201ea18ad4"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "20.04.202205050"
  }


  computer_name                   = "Master"
  admin_username                  = "linpo"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "linpo"
    public_key = azurerm_ssh_public_key.K8S.public_key
  }
}


# create ip slave1
resource "azurerm_public_ip" "Slave1K8S" {
  name                = "Slave1-ip"
  resource_group_name = azurerm_resource_group.K8S.name
  location            = azurerm_resource_group.K8S.location
  allocation_method   = "Static"
}

# Create network interface Slave1
resource "azurerm_network_interface" "Slave1K8S" {
  name                = "Slave1nic"
  location            = azurerm_resource_group.K8S.location
  resource_group_name = azurerm_resource_group.K8S.name

  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = azurerm_subnet.K8S.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.Slave1K8S.id
  }
}

# Connect the security group to the network interface for Slave1
resource "azurerm_network_interface_security_group_association" "Slave1K8S" {
  network_interface_id      = azurerm_network_interface.Slave1K8S.id
  network_security_group_id = azurerm_network_security_group.K8S.id
}

  # Create virtual machine Slave1
  resource "azurerm_linux_virtual_machine" "Slave1K8S" {
  name                  = "Slave1MAchine"
  location              = azurerm_resource_group.K8S.location
  resource_group_name   = azurerm_resource_group.K8S.name
  network_interface_ids = [azurerm_network_interface.Slave1K8S.id]
  size                  = "Standard_B2s"

  os_disk {
    name                 = "Slave1Disk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "20.04.202205050"
  }


  computer_name                   = "Slave1"
  admin_username                  = "linpo"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "linpo"
    public_key = azurerm_ssh_public_key.K8S.public_key
  }

}

# create ip Slave2
resource "azurerm_public_ip" "Slave2K8S" {
  name                = "Slave2-ip"
  resource_group_name = azurerm_resource_group.K8S.name
  location            = azurerm_resource_group.K8S.location
  allocation_method   = "Static"
}

# Create network interface Slave2
resource "azurerm_network_interface" "Slave2K8S" {
  name                = "Slave2nic"
  location            = azurerm_resource_group.K8S.location
  resource_group_name = azurerm_resource_group.K8S.name

  ip_configuration {
    name                          = "ipconfig1"
    subnet_id                     = azurerm_subnet.K8S.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.Slave2K8S.id
  }
}

# Connect the security group to the network interface for Slave2
resource "azurerm_network_interface_security_group_association" "Slave2K8S" {
  network_interface_id      = azurerm_network_interface.Slave2K8S.id
  network_security_group_id = azurerm_network_security_group.K8S.id
}

  # Create virtual machine Slave2
  resource "azurerm_linux_virtual_machine" "Slave2K8S" {
  name                  = "Slave2MAchine"
  location              = azurerm_resource_group.K8S.location
  resource_group_name   = azurerm_resource_group.K8S.name
  network_interface_ids = [azurerm_network_interface.Slave2K8S.id]
  size                  = "Standard_B1s"

  os_disk {
    name                 = "Slave2Disk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "20.04.202205050"
  }


  computer_name                   = "Slave2"
  admin_username                  = "linpo"
  disable_password_authentication = true

  admin_ssh_key {
    username   = "linpo"
    public_key = azurerm_ssh_public_key.K8S.public_key
  }

}

